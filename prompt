# vim: set filetype=sh:

# Color codes for the prompt
COLOR_RED="\[\033[0;31m\]"
COLOR_LIGHT_RED="\[\033[1;31m\]"
COLOR_BLUE="\[\033[0;34m\]"
COLOR_WHITE="\[\033[1;37m\]"
COLOR_LIGHT_BLUE="\[\033[1;34m\]"
COLOR_RESET="\[\033[0m\]"

# Check if current directory is a git repo and check it's status
function git_status {
	GIT_STATUS=$(git rev-parse --abbrev-ref HEAD 2>/dev/null | tr -d '\n')
	if [[ -n $GIT_STATUS ]] ; then
		echo " $GIT_STATUS"
	fi
}

# Change prompt color for root user
function user_status {
	if [[ $UID -gt 0 ]] ; then
		USER_COLOR="\[\033[0;37\]"
	else
		USER_COLOR="\[\033[0;31m\]"
	fi
}

function hostname_status {
	echo "$HOSTNAME"
}

# Display the error code of last command
function error_status {
	error_code=$?
	if [[ $error_code -ne 0 ]] ; then
		echo "$error_code "
	fi
}

# Check for color support
if [[ -x /usr/bin/tput ]] && tput setaf 1>&/dev/null; then
	color_prompt=yes
else
	color_prompt=
fi

if [[ -n $color_prompt ]] ; then
	PS1="$COLOR_RED\$(error_status)$COLOR_RESET\$(user_status)$USER_COLOR\u@$COLOR_LIGHT_BLUE\$(hostname_status)$COLOR_RESET \W$COLOR_WHITE\$(git_status)$COLOR_RESET \$ "
else
	PS1="\$(error_status)\$(user_status)@\$(hostname_status)\W\$\$(git_status)"
fi
