# vim: set filetype=sh:

function mkcd {
	mkdir "$1"
	cd "$1"
}

function cds {
	cd "$1"
	ls
}

function launch {
	nohup "$@" 2>/dev/null & disown
}

function bak {
	cp -r "$1" "$1".bak
}

function lsza {
	for f in ./* ; do
		du -sh "$f"
	done
}

function lolman {
	man "$1" | lolcat -f | less -RSMsi
}

function pip {
	if [[ -z $VIRTUAL_ENV ]] ; then
		echo "Don't run pip outside the virtualenv"
		return 1
	else
		$(which pip) "$@"
	fi
}

function activate {
	if [[ -n $VIRTUAL_ENV ]] ; then
		echo "You're already inside a virtualenv"
		return 1
	else
		source "$1"/bin/activate
	fi
}

function diffstr {
	if [[ "$1" == "$2" ]] ; then
		echo 'Ok'
	else
		echo 'No match'
	fi
}

function mandie {
	section="$1"
	search_string="$2"
	links "https://linux.die.net/man/$section/$search_string"
}

function setup_venv {
	virtualenv -p "$1" "$2"
	activate "$2"
	pip install jedi neovim yapf pylint
}
