# vim: set filetype=sh:
# If not running interactively, don't do anything
case $- in
    *i*) ;;
      *) return;;
esac

# Append to the history file, don't overwrite it
shopt -s histappend

# Check window size
shopt -s checkwinsize

# source different settings
for file in aliases functions environment prompt history readline ; do
	if [[ -f "$HOME/.config/bash/$file" ]] ; then
		source "$HOME/.config/bash/$file"
	else
		echo "Couldn't source $file"
	fi
done

PATH=/opt/puppetlabs/bin/:$HOME/perl5/bin:$HOME/.gem/ruby/2.4.0/bin/:$HOME/bin/:$PATH:/usr/lib/plan9/bin/

# Enable bash completion
if ! shopt -oq posix; then
	if [[ -f /usr/share/bash-completion/bash_completion ]]; then
		source /usr/share/bash-completion/bash_completion
	elif [[ -f /etc/bash_completion ]]; then
		source /etc/bash_completion
	fi
fi
